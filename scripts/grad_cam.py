import cv2
import numpy as np
import tensorflow as tf

IMAGE_PATH = r'.\cat_dog.png'
LAYER_NAME = 'block5_conv3' #The convolutional layer used for the gradCAM.
CAT_CLASS_INDEX = 281
BASELINE_LAYER = 'block5_conv3' 

def gradC(path, layer, class_index):
    img = tf.keras.preprocessing.image.load_img(path, target_size=(224, 224))
    img = tf.keras.preprocessing.image.img_to_array(img)

    model = tf.keras.applications.vgg16.VGG16(weights='imagenet', include_top=True)

    grad_model = tf.keras.models.Model([model.inputs], [model.get_layer(layer).output, model.output])

    with tf.GradientTape() as tape:
        conv_outputs, predictions = grad_model(np.array([img]))
        loss = predictions[:, class_index]

    output = conv_outputs[0]
    grads = tape.gradient(loss, conv_outputs)[0]

    gate_f = tf.cast(output > 0, 'float32')
    gate_r = tf.cast(grads > 0, 'float32')
    guided_grads = gate_f * gate_r * grads

    weights = tf.reduce_mean(guided_grads, axis=(0, 1))

    cam = np.ones(output.shape[0: 2], dtype = np.float32)

    for i, w in enumerate(weights):
        cam += w * output[:, :, i]

    cam = cv2.resize(cam.numpy(), (224, 224))
    cam = np.maximum(cam, 0)
    heatmap = (cam - cam.min()) / (cam.max() - cam.min())

    cam = cv2.applyColorMap(np.uint8(255*heatmap), cv2.COLORMAP_JET)

    output_image = cv2.addWeighted(cv2.cvtColor(img.astype('uint8'), cv2.COLOR_RGB2BGR), 0.5, cam, 1, 0)
    return output_image

def main():
    IMAGE_PATH = r'.\cat_dog.png'
    LAYER_NAME = 'block5_conv3'
    CAT_CLASS_INDEX = 281
    output_image = gradC(path=IMAGE_PATH, layer=LAYER_NAME, class_index=CAT_CLASS_INDEX)
    cv2.imwrite('cam.png', output_image)
    
if __name__ == "__main__":
    main()