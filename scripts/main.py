import tensorflow as tf
import pandas as pd
import os
import sys
import glob
import argparse
import grad_cam as gc
import save as sv

DATA = os.path.join("..", "data")
MODELS = os.path.join("..", "models") #Library with models
BASELINE = os.path.join(MODELS, 'bl_model.h5') #Baseline model

IMG_SIZE = 224 # Specify height and width of image to match the input format of the model
CHANNELS = 3 # Keep RGB color channels to match the input format of the model
BATCH_SIZE = 32 # Training batch size
AUTOTUNE = tf.data.experimental.AUTOTUNE #Adapt preprocessing and prefetching dynamically to reduce GPU and CPU idle time

def ignore_binary_crossentropy(y_true, y_pred):
    '''
    Ignore the labels while calculating binary crosentropy if the true value is -1. Needs to be imported to load the model.
    Args:
        y_true: true labels [0/1/-1].
        y_pred: predictions, a probability [0,1].
    Returns:
        binary_crossentropy: the binary crossentropy of the batch.
    '''
        
    y_true = tf.reshape(y_true, [-1]) 
    y_pred = tf.reshape(y_pred, [-1])
    y_true = tf.multiply(y_true, tf.cast(tf.not_equal(y_true, -1), tf.float32))
    y_pred = tf.multiply(y_pred, tf.cast(tf.not_equal(y_true, -1), tf.float32))
    return tf.keras.losses.binary_crossentropy(y_true, y_pred)

def parse_args():
    '''
    Function that parses the arguments recieved in console.
    Returns:
        X: a list with image paths.
        modelpath: the path to the pretrained model.
        images: whether GradCAM will be applied.
        anomaly: which anomaly which must be analyzed.
    '''

    ap = argparse.ArgumentParser()
    ap.add_argument("file", type=str, help="Either a file in csv format specified or a folder with the images.")
    ap.add_argument("-m", "--model", required=False, type=str, help="The model used for prediction, if not specified bl_model will be used.")
    ap.add_argument("-i", "--images", required=False, action="store_true", help="Whether to get the results with images or not. yes/[no]")
    ap.add_argument("-a", "--anomaly", required=False,  type=str, help="Anomaly searched.")
    args = vars(ap.parse_args())

    filepath = args['file']
    modelpath = args['model']
    images = args['images']
    anomaly = args['anomaly']

    if (filepath[-4:] == ".csv"):
        X = read_the_csv(filepath)
    else: 
        X = read_folder(filepath)

    if (modelpath == None):
        modelpath = BASELINE

    if (anomaly == None):
        anomaly = 'ALL'

    return X, modelpath, images, anomaly

def read_folder(folder):
    '''
    Read images in a folder.
    Args:
        filename: string representing path to the folder.
    Returns:
        an array with strings representing path to image.
    '''

    path = os.path.join(folder, '*')
    return glob.glob(path)

def parse_function(filename):
    '''
    Function that returns a tuple of normalized image array.
    Args:
        filename: string representing path to an image.
    Returns:
        an array with strings representing path to image.
    '''

    # Read an image from a file
    image_string = tf.io.read_file(filename)
    # Decode it into a dense vector
    image_decoded = tf.image.decode_jpeg(image_string, channels=CHANNELS)
    # Resize it to fixed shape
    image_resized = tf.image.resize(image_decoded, [IMG_SIZE, IMG_SIZE])
    # Normalize it from [0, 255] to [0.0, 1.0]
    image_normalized = image_resized / 255.0
    return image_normalized

def create_dataset(filenames):
    '''
    Load and parse dataset.
    Args:
        filenames: list of image paths.
    Returns:
        a tensorflow dataset.
    '''

    # Create a first dataset of file paths and labels
    dataset = tf.data.Dataset.from_tensor_slices(filenames)
    # Parse and preprocess observations in parallel
    dataset = dataset.map(parse_function, num_parallel_calls=AUTOTUNE)  
    # Batch the data for multiple steps
    dataset = dataset.batch(BATCH_SIZE)
    # Fetch batches in the background while the model is predicting .
    dataset = dataset.prefetch(buffer_size=AUTOTUNE)
    return dataset

def load_model(modelpath):
    '''
    Load saved model.
    Args:
        modelpath: path to the saved keras model.
    Returns:
        a keras model.
    '''

    try:
        #Load the saved model
        model = tf.keras.models.load_model(modelpath , custom_objects={'ignore_binary_crossentropy': ignore_binary_crossentropy})
    except Exception as e:
        print("Model not loaded:")
        print('Exception: %s' % str(e))
        sys.exit(1)

    return model

def batch_GC(paths, pred, class_index):
    '''
    Applies GradCAM to a batch of images.
    Args:
        paths: paths to the images.
        pred: predictions achieved for each image.
        class_index: class to which gradCAM will be applied.
    Returns:
        images with GradCAM applied.
    '''

    pass
    # GC_images = []

    # for path in paths:
    #     output_image = gc.gradC(path, BASELINE_LAYER, class_index)
    #     GC_images.append(output_image)

    # return GC_images

def prediction(model, ds):
    '''
    A function that predicts based on the model. 

    Args:
        data: a tf.data tensor with the images.
        model: the model that will be used to predict.
    Returns:
        the predictions.
    '''

    model = load_model(model)
    pred = model.predict(ds)
    return pred

def read_the_csv(filepath):
    '''
    Read the filepaths in the csv.

    Args:
        filepath: a csv file.
    Returns:
        a list of image paths.
    '''

    try:
        csv = pd.read_csv(filepath)

    except Exception as e:
        print('Exception: %s' % str(e))
        sys.exit(1)
    feature = "Path"
    X = csv[feature].apply(lambda filename : os.path.join(DATA,filename)).to_numpy()
    return X


def main():
    '''
    Given arguments in console uploads the paths to the images with their respective predictions (and GradCAM images).
    Console args:
        file:  Either a file in csv format specified or a folder with the images.
        -m, --model: The model used for prediction, if not specified bl_model will be used.
        -i, --images: Whether to get the results with images or not. yes/[no]
        -a, --anomaly: Anomaly searched. {Atelectasis, Cardiomegaly, Consolidation, Edema, Pleural Effusion}
    
    '''

    X, modelpath, images, anomaly = parse_args()
    ds = create_dataset(X)
    pred = prediction(modelpath, ds)
 
    if (images):
        batch_GC(paths=ds, pred=pred, class_index= anomaly)
        sv.upload_to_db(X, pred)
        
    else: 
        sv.upload_to_db(X, pred)

if __name__ == "__main__":
    main()