import pymongo
from pymongo import MongoClient
import json 


def format(img, pred):
    pred
    predictions = []
    anomalies = ["no_finding", "enlarged_cardiomediastinum", "cardiomegaly", "lung_opacity", "lung_lesion", "edema", "consolidation", "pneumonia", "atelectasis", "pneumothorax", "pleural_effusion", "pleural_other", "fracture", "support_devices"]


    for i in range(len(pred)):
        d = {}
        d["images"] =img[i]
        j = 0
        for anomaly in anomalies:
            d[anomaly] = pred[i, j].tolist()
            j +=1
        predictions.append(d)
    return predictions


def upload_to_db(img, pred, GC_images=None):
    '''
    Given the paths and the predictions for each image, uploads both to
    "CheXpert" database "Prediction" collection.

    Args:
       ds: data structure with paths to the images.
       pred: array of predictions for each image.
    '''
    client = MongoClient()
    db = client.cheXpert
    predictions = db.predictions
    predictions.drop()
    predict = format(img, pred)
    predictions.insert(predict)