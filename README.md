# Anomaly detection pipeline with CheXpert Dataset#
*A repository created for the end-of-degree project in collaboration with Eurohelp Consulting*

Author: Jon Ander Gonzalez

This GitHub repository is composed of:

1. A jupyter notebook used for visualization and analysis of the CheXpert Dataset.

2. A jupyter notebook used for the training, testing and parameter tunning.

3. An end-to-end pipeline using those models.

4. A web app for easier comprehension fo the results. 

# Instalation. 

This project has been created with an anaconda virtual enviroment using python 3.7, to avoid problems, same configuration is adviced.

To run the notebooks, the pipeline and the app you will need to run **conda env update -n <env-name> -f requirements.yml**.

As the tensorflow version is 2.1, check system and hardware requirements at [https://www.tensorflow.org/install/pip](https://www.tensorflow.org/install/pip).

If running on windows check that the [Visual C++](https://support.microsoft.com/es-es/help/2977003/the-latest-supported-visual-c-downloads) package is up-to-date.

If running with GPU check [https://www.tensorflow.org/install/gpu](https://www.tensorflow.org/install/gpu)

Before running any notebook, the pipeline, or the app, you will need to add the **CheXpert-v1.0-small** folder inside a **data** folder at the same directory level as the notebooks. The **dataset** (the smaller dataset size is 11 GB) can be requested on the [chexpert website](https://stanfordmlgroup.github.io/competitions/chexpert/) of the competition.


# 1. Visualization

The dataset analysis is in the notebook **dataVis.ipynb**. Data loading and visualization to get a grasp of the problem, useful to know what exactly is the training input of the models used. 

# 2. Training

The training of the models and parameter tunning is in the notebook **training.ipynb**

The notebook consists of:

1. Data preprocessing.

2. Training the model and parameter tunning.

3. Comparison of results between preprocessing techniques and parameters.


The models are saved in the **models** folder.

# 4. Pipeline

TODO

# References

1. Pham, Hieu H., et al. "Interpreting chest X-rays via CNNs that exploit disease dependencies and uncertainty labels." arXiv preprint arXiv:1911.06475 (2019). [[Arxiv:1911.06475]](https://arxiv.org/pdf/1911.06475.pdf)

2. Selvaraju, Ramprasaath R., et al. "Grad-cam: Visual explanations from deep networks via gradient-based localization." Proceedings of the IEEE international conference on computer vision. 2017. [[Arxiv:1610.02391]](https://arxiv.org/abs/1610.02391)